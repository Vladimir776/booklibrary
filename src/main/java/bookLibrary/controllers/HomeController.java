package bookLibrary.controllers;

import bookLibrary.model.Book;
import bookLibrary.service.BookLibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class HomeController {
    @Autowired
    private BookLibraryService bookLibraryService;

    @RequestMapping(value = {"/","/home"}, method = RequestMethod.GET)
    ModelAndView getHomePage(Model model){
        List<Book> books = bookLibraryService.getAllBooks();
        ModelAndView homeMAV = new ModelAndView();
        homeMAV.setViewName("home");
        homeMAV.addObject("booksList" , books);
        return homeMAV;
    }
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPage(@PathVariable("id") int id) {
        Book book = bookLibraryService.getBookById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/edit");
        modelAndView.addObject("book", book);
        return modelAndView;

    }

    public static void main(String[] args) {

    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ModelAndView editFilm(@ModelAttribute("book") Book book) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        bookLibraryService.editBook(book);
        return modelAndView;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deletePage(@PathVariable("id") int id) {
        Book book = bookLibraryService.getBookById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        bookLibraryService.deleteBook(book);
        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("edit");
        return modelAndView;
    }
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addFilm(@ModelAttribute("book") Book book) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        bookLibraryService.addBook(book);
        return modelAndView;
    }
}
