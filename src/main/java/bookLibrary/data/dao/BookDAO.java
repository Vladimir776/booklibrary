package bookLibrary.data.dao;


import bookLibrary.model.Book;

import java.util.List;

public interface BookDAO {

    List<Book> getAllBooks();
    Book addBook(Book book);
    Book editBook(Book book);
    Book getBookById(int id);
    Book deleteBook(Book book);
    Book getTitleImageById(int id);
}
