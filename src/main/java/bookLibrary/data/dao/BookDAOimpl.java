package bookLibrary.data.dao;

import bookLibrary.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
public class BookDAOimpl implements BookDAO {
    private SessionFactory sessionFactory;


    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Book> getAllBooks() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("From Book").list();
    }

    @Override
    public Book addBook(Book book) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(book);
        return book;
    }

    @Override
    public Book editBook(Book book) {
        Session session = sessionFactory.getCurrentSession();
        session.update(book);
        return book;
    }

    @Override
    public Book getBookById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Book.class, id);
    }

    @Override
    public Book deleteBook(Book book) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(book);
        return book;
    }

    @Override
    public Book getTitleImageById(int id) {
        return null;
    }
}
