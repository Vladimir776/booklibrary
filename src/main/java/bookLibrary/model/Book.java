package bookLibrary.model;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Entity
@Table(name = "books")
public class Book implements Serializable {

    @Id
    @Column(name = "id")
    private int id;             //идентификатор книги в БД;

    @Column(name = "title")
    private String title;       //название книги

    @Column(name = "description")
    private String description; //краткое описание о чем книга

    @Column(name = "author")
    private String author;      //фамилия и имя автора

    @Column(name = "isbn")
    private String isbn;        //ISBN  книги

    @Column(name = "printYear")
    private int printYear;       //в каком году напечатана книга

    @Column(name = "readAlready")
    boolean readAlready;         //читал ли кто-то эту книгу. Это булевополе.

    public Book() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getPrintYear() {
        return printYear;
    }

    public void setPrintYear(int printYear) {
        this.printYear = printYear;
    }

    public boolean isReadAlready() {
        return readAlready;
    }

    public void setReadAlready(boolean readAlready) {
        this.readAlready = readAlready;
    }

    @Override
    public String toString() {
        return
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", printYear=" + printYear ;
    }
}
