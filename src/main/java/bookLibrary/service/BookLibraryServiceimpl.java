package bookLibrary.service;

import bookLibrary.data.dao.BookDAO;
import bookLibrary.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class BookLibraryServiceimpl implements BookLibraryService {

    @Autowired
    private BookDAO bookDAO;

    @Override
    @Transactional
    public List<Book> getAllBooks() {
        return bookDAO.getAllBooks();
    }

    @Override
    @Transactional
    public Book addBook(Book book) {
        return bookDAO.addBook(book);
    }

    @Override
    @Transactional
    public Book editBook(Book book) {
        return bookDAO.editBook(book);
    }

    @Override
    @Transactional
    public Book getBookById(int id) {
        return bookDAO.getBookById(id);
    }

    @Override
    @Transactional
    public Book deleteBook(Book book) {
        return bookDAO.deleteBook(book);
    }
}
