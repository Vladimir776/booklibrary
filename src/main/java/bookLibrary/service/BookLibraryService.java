package bookLibrary.service;

import bookLibrary.model.Book;

import java.util.List;

public interface BookLibraryService {
    List<Book> getAllBooks();
    Book addBook(Book book);
    Book editBook(Book book);
    Book getBookById(int id);
    Book deleteBook(Book book);
}
