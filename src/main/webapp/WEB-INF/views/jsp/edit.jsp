<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <c:if test="${empty book.title}">
        <title>Add</title>
    </c:if>
    <c:if test="${!empty book.title}">
    <title>Edit</title>
    </c:if>
</head>
<body>
<c:if test="${!empty book.title}">
    <c:url value="/edit" var="var"/>
</c:if>
<c:if test="${empty book.title}">
    <c:url value="/add" var="var"/>
</c:if>
<form action="${var}" method="POST">
    <c:if test="${!empty book.title}">
    <input type="hidden" name="id" value="${book.id}">
    </c:if>
    <label for="title">Title</label>
    <input type="text" name="title" id="title">
    <label for="author">Author</label>
    <input type="text" name="author" id="author">
    <label for="description">Description</label>
    <input type="text" name="description" id="description">
    <label for="isbn">Isbn</label>
    <input type="text" name="isbn" id="isbn">
    <label for="printYear">PrintYear </label>
    <input type="text" name="printYear" id="printYear"
           value="${not empty book.printYear?book.printYear:'1900'}">
    <label for="readAlready">ReadAlready</label>
    <input type="text" name="readAlready" id="readAlready"
           value="${not empty book.readAlready?book.readAlready:'false'}">
    <c:if test="${!empty book.title}">
        <input type="submit" value="Edit book">
    </c:if>
    <c:if test="${empty book.title}">
        <input type="submit" value="Add book">
    </c:if>
</form>
</body>
</html>